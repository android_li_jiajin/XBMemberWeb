/*
 * @Author: xugj
 * @Date: 2020-07-23 16:33:27
 * @LastEditTime: 2020-07-23 18:00:35
 * @LastEditors: Please set LastEditors
 * @Description: 修改管理员密码
 * @FilePath: /XBMemberWeb/js/modify-manager-pwd.js
 */ 

 function modifyPwd(){
    // 验证旧秘密
    let oldPwdInput = $('.old-pwd');
    let oldPwdInputVal = oldPwdInput.val().trim();
    if(!oldPwdInputVal){
        removeErrorMsg(oldPwdInput);
        showMsg("请输入旧密码", oldPwdInput, oldPwdInput)
        return;
    }else if(oldPwdInputVal.length > 20 || oldPwdInputVal.length < 8){
        removeErrorMsg(oldPwdInput);
        showMsg("请输入正确的旧密码", oldPwdInput, oldPwdInput)
        return;
    }else{
        removeErrorMsg(oldPwdInput);
    }

    // 验证新密码
    let newPwdInput = $('.new-pwd');
    let newPwdInputVal = newPwdInput.val().trim();
    if(!newPwdInputVal){
        removeErrorMsg(newPwdInput);
        showMsg("请输入新密码", newPwdInput, newPwdInput)
        return;
    }else if(newPwdInputVal.length > 20 || newPwdInputVal.length < 8){
        removeErrorMsg(newPwdInput);
        showMsg("请输入正确的新密码", newPwdInput, newPwdInput)
        return;
    }else{
        removeErrorMsg(newPwdInput);
    }

    // 验证确认密码
    // 验证确认密码
    let confirmPwdInput = $('.comfir-pwd');
    let confirmPwdInputVal = confirmPwdInput.val().trim();
    if(!confirmPwdInputVal){
        removeErrorMsg(confirmPwdInput);
        showMsg("请输入确认密码", confirmPwdInput, confirmPwdInput)
        return;
    }else if(confirmPwdInputVal.length > 20 || confirmPwdInputVal.length < 8){
        removeErrorMsg(confirmPwdInput);
        showMsg("请输入正确的确认密码", confirmPwdInput, confirmPwdInput)
        return;
    } else if(confirmPwdInputVal != newPwdInputVal){
        removeErrorMsg(confirmPwdInput);
        showMsg("确认密码必须和新密码一致", confirmPwdInput, confirmPwdInput)
        return;
    } else{
        removeErrorMsg(confirmPwdInput);
    }

    showToast('修改成功', 1, 2, '../login.html')
 }