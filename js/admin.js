/*
 * @Description: 
 * @Author: lijiajin
 * @Date: 2020-07-06 09:04:53
 * @LastEditTime: 2020-07-23 18:01:29
 * @LastEditors: Please set LastEditors
 */
$(function () {

    // 滚动条
    $('.nano').nanoScroller();

    var layer;
    layui.use('layer', function () {
        layer = layui.layer;
    });

    // 左侧菜单展开、收起
    $(".admin .menu-logo .menu-btn").on("click", function () {
        $(this).toggleClass("iconmenufold-right");
        $(".body .menu").toggleClass("open");
        $(this).closest(".top").toggleClass("open");
        $(".body").toggleClass("menu-open").toggleClass("menu-close");
    })

    // 左侧菜单展开收起子级分类
    $(".admin").on("click", ".body.menu-open .menu .list .text", function () {
        $(this).closest(".title").toggleClass("show").closest("li").addClass("active").siblings("li").removeClass("active").find(".title").removeClass("show");
        $('.nano').nanoScroller();
        if (!$(this).closest(".title").hasClass("has-child")) {
            $(".menu").find(".title").not(".has-child").find(".txt").removeClass("current");
            $(this).find(".txt").addClass("current");
        }
    })

    // 接收子页面消息
    window.addEventListener('message', function (e) {
        if (e.data.msg.type == "show_pic") {
            showPicDialog(e.data.msg.curr, e.data.msg.src);
        }
    }, false);

    $(".show-pic-dialog").on("click", ".prev", function () {
        var curr = parseInt($(this).siblings("img").attr("data-curr"));
        if (curr == 0) {
            curr = lookPicSrcs.length - 1;
        } else {
            curr = curr - 1;
        }
        $(".show-pic-dialog").show().find("img").attr("src", lookPicSrcs[curr]).attr("data-curr", curr);
    })

    $(".show-pic-dialog").on("click", ".next", function () {
        var curr = parseInt($(this).siblings("img").attr("data-curr"));
        if (curr == lookPicSrcs.length - 1) {
            curr = 0;
        } else {
            curr = curr + 1;
        }
        $(".show-pic-dialog").show().find("img").attr("src", lookPicSrcs[curr]).attr("data-curr", curr);
    })

    $(".show-pic-dialog").on("click", ".close", function () {
        $(this).siblings("img").attr("src", "").attr("data-curr", "0").closest(".show-pic-dialog").hide();
        lookPicSrcs = [];
    })

    // var datas = {
    //   "code": 200,
    //   "msg": {
    //     "1": [
    //       { "field": "name", "msg": "\u8bf7\u8f93\u5165\u540d\u79f0" },
    //       { "field": "email", "msg": "\u8bf7\u8f93\u5165\u90ae\u7bb1" }
    //     ],
    //     "3": [
    //       { "field": "email3", "msg": "\u8bf7\u8f93\u5165\u90ae\u7bb1" },
    //       { "field": "content3", "msg": "\u8bf7\u8f93\u5165\u5185\u5bb9" }
    //     ]
    //   },
    //   "data": []
    // }

    // for(var i in datas.msg){
    //     console.log(i);
    //     console.log(datas.msg[i])
    // }

    // 多选
    $(".check-box-div input[type='checkbox']").change(function () {
        if ($(this).prop("checked")) {
            $(this).closest(".icon").addClass("iconfuxuankuangxuanzhong").removeClass("iconduoxuanweixuanzhong");
        } else {
            $(this).closest(".icon").addClass("iconduoxuanweixuanzhong").removeClass("iconfuxuankuangxuanzhong");
        }
    })

    $("input[type='text']").on("blur", function () {
        $(this).closest(".item").find(".m-error").remove();
        $(this).closest(".item").find(".red-border").removeClass("red-border");
    })

    // 登录
    $(".wrap-login .login-box ul li a").on("click", function () {
        $(this).addClass("active").closest("li").siblings("li").find("a").removeClass("active");
        if ($(this).closest("li").hasClass("phone")) {
            $(".phone-box").show().find("input[type='text']").prop("disabled", false);
            $(".email-box").hide().find("input[type='text']").prop("disabled", true);
        } else {
            $(".phone-box").hide().find("input[type='text']").prop("disabled", true);
            $(".email-box").show().find("input[type='text']").prop("disabled", false);
        }
        $("#login_form").find(".m-error").remove();
        $("#login_form").find(".red-border").removeClass("red-border");
    })
    var login_member_able = true;
    $(".wrap-login .login-box .login-btn").on("click", function () {
        if (!login_member_able) {
            return;
        }
        var login_action = $('#login_form').attr('action');

        var account;
        var remember = $(".checkbox-input").prop("checked") ? 1 : 0;
        var type = 2; // 1:邮箱 2：手机号

        var params = {};
        if ($(".phone-box").is(':hidden')) {
            account = $("#email").val();
            type = 1;
            params.email = account;
        } else {
            account = $("#phone").val();
            type = 2;
            params.mobile = account;
        }
        var password = $("#pwd").val();
        if (!checkLoginForm(account, password)) {
            return;
        }
        params.password = password;
        params.remember = remember;
        params.type = type;
        $.ajax({
            type: "POST",
            url: login_action,
            data: params,
            dataType: "json",
            success: function (data) {
                if (data.code == 200) {
                    showToast(data.msg, 1, 2, data.data.url)
                } else {
                    showToast(data.msg, 3);
                }
            },
            error: function () {
                showToast("请求失败", 2);
            },
            beforeSend: function () {
                login_member_able = false;
                showLoading();
            },
            complete: function () {
                login_member_able = true;
                hideLoading();
            }
        });
    })

})

function checkLoginForm(account, pwd) {
    $("#login_form").find(".m-error").remove();
    $("#login_form").find(".red-border").removeClass("red-border");
    var okable = true;
    if ($(".phone-box").is(':hidden')) {
        if (!account) {
            showMsg("请输入邮箱", $("#email").closest(".input"), $("#email"));
            okable = false;
        } else if (!checkEmail(account)) {
            showMsg("请输入正确的邮箱", $("#email").closest(".input"), $("#email"));
            okable = false;
        }
    } else {
        if (!account) {
            showMsg("请输入手机号", $("#phone").closest(".input"), $("#phone"));
            okable = false;
        } else if (!checkPhone11(account)) {
            showMsg("请输入正确的手机号", $("#phone").closest(".input"), $("#phone"));
            okable = false;
        }
    }
    if (!pwd) {
        showMsg("请输入密码", $("#pwd").closest(".input"), $("#pwd"));
        okable = false;
    }
    return okable;
}

// 请填写正确的邮箱
function checkEmail(value) {
    // var test = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
    if (value.indexOf("@") == -1) {
        okable = false;
    }
    return true;
}

// 请填写手机号  11位数字
function checkPhone11(value) {
    // var test = /^\d{11}|\d{13}$/;
    var test = /^\d{11}$/;

    return test.test(value);
}

/**
 * 提示信息
 * @param msg
 * @param icon  1:成功    2：失败    3：其他
 * @param type  1：提示不跳转   2：提示跳转链接  3：提示刷新页面
 * @param url
 */
function showToast(msg, icon, type, url) {
    if (!layer) return;
    if (!arguments[1]) icon = 3;
    if (!arguments[2]) type = 1;
    if (!arguments[3]) url = "";
    if (type == 1) { // 提示不跳转
        layer.msg(msg, {
            time: 1500,
            icon: icon,
            skin: "show-toast-0" + icon,
        });

    } else if (type == 2) { // 提示跳转链接
        layer.msg(msg, {
            time: 1500,
            icon: icon,
            skin: "show-toast-0" + icon,
            end: function () {
                location.href = url;
            }
        });
    } else if (type == 3) { // 提示刷新页面
        layer.msg(msg, {
            time: 1500,
            icon: icon,
            skin: "show-toast-0" + icon,
            end: function () {
                location.reload();
            }
        });
    } else {
        layer.msg(msg);
    }

}

/**
 * 错误信息
 * @param msg 提示信息
 * @param obj  插入节点
 * @param redObj  红框元素
 */
function showMsg(msg, obj, redObj) {
    // $(obj).next()
    $(obj).after("<label class='m-error'>" + msg + "</label>");
    if (redObj) {
        $(redObj).addClass("red-border");
    }
}


// 查看图片
var lookPicSrcs = [];

function showPicDialog(curr, src) {
    lookPicSrcs = src;
    if (lookPicSrcs.length == 1) {
        $(".show-pic-dialog .box .box-btn").hide()
    } else {
        $(".show-pic-dialog .box .box-btn").show()
    }
    $(".show-pic-dialog").show().find("img").attr("src", lookPicSrcs[curr]).attr("data-curr", curr);
}

// *********************登录相关js*********************
var currType = 'phone';

function emailOrPhone() {
    if (currType == 'email') {
        $('.change-email-phone').text('用邮箱找回');
        $('#div-email').hide();
        $('#div-phone').show();
        $('.ver-code-input').val("");
        currType = 'phone';
    } else {
        $('.change-email-phone').text('用手机找回');
        $('#div-email').show();
        $('#div-phone').hide();
        $('.ver-code-input').val("");
        currType = 'email';
    }
}

// 发送验证码倒计时功能

function time(o) {
    if (!formCheck()) {
        return;
    }
    ;
    countdown(o);

}

var wait = 60;

function countdown(o) {
    if (wait == 0) {
        o.removeAttribute("disabled");
        o.value = "发送验证码";
        wait = 60;
    } else {
        o.setAttribute("disabled", true);
        o.value = "重新发送(" + wait + ")";
        wait--;
        setTimeout(function () {
                countdown(o)
            },
            1000)
    }
}

function next_step() {
    formCheck();
    //验证码
    let verCodeNode = $('.ver-code-input');
    let verCodeDiv = $('.ver-code-btn');
    if (!verCodeNode.val()) {
        removeErrorMsg(verCodeDiv, verCodeNode);
        showMsg("请输入验证码", verCodeDiv, verCodeNode)
        return;
    } else if (verCodeNode.val().trim().length != 6) {
        removeErrorMsg(verCodeDiv, verCodeNode);
        showMsg("请输入正确的验证码", verCodeDiv, verCodeNode)
        return;
    } else {
        removeErrorMsg(verCodeDiv, verCodeNode);
    }

    let errorNode = $('form').find(".m-error");
    if (errorNode.length != 0) {
        return;
    }
    window.location.href = "../login/reset-password.html";
}


function formCheck() {
    let flag = false;
    if (currType == 'email') {
        let emailNode = $("#input-email");
        if (!emailNode.val()) {
            removeErrorMsg(emailNode);
            showMsg("请输入邮箱地址", emailNode, emailNode)
            flag = false;
        } else if (checkEmail(emailNode.val())) {
            removeErrorMsg(emailNode);
            showMsg("请输入正确的邮箱", emailNode, emailNode)
            flag = false;
        } else {
            removeErrorMsg(emailNode);
            flag = true;
        }
    } else {
        let phonelNode = $("#input-phone");
        if (!phonelNode.val()) {
            removeErrorMsg(phonelNode);
            showMsg("请输入手机号", phonelNode, phonelNode)
            flag = false;
        } else if (!checkPhone(phonelNode.val())) {
            removeErrorMsg(phonelNode);
            showMsg("请输入正确的手机号", phonelNode, phonelNode)
            flag = false;
        } else {
            removeErrorMsg(phonelNode);
            flag = true;
        }
    }
    return flag;
}

// 重置界面js

function resetPwd() {
    // 验证新秘密
    let newPwdInput = $('.new-pwd').find('input');
    let newPwdInputVal = newPwdInput.val().trim();
    if (!newPwdInputVal) {
        removeErrorMsg(newPwdInput);
        showMsg("请输入新密码", newPwdInput, newPwdInput)
        return;
    } else if (newPwdInputVal.length > 20 || newPwdInputVal.length < 8) {
        removeErrorMsg(newPwdInput);
        showMsg("请输入正确的新密码", newPwdInput, newPwdInput)
        return;
    } else {
        removeErrorMsg(newPwdInput);
    }

    // 验证确认密码
    let confirmPwdInput = $('.confirm-pwd').find('input');
    let confirmPwdInputVal = confirmPwdInput.val().trim();
    if (!confirmPwdInputVal) {
        removeErrorMsg(confirmPwdInput);
        showMsg("请输入确认密码", confirmPwdInput, confirmPwdInput)
        return;
    } else if (confirmPwdInputVal.length > 20 || confirmPwdInputVal.length < 8) {
        removeErrorMsg(confirmPwdInput);
        showMsg("请输入正确的确认密码", confirmPwdInput, confirmPwdInput)
        return;
    } else if (confirmPwdInputVal != newPwdInputVal) {
        removeErrorMsg(confirmPwdInput);
        showMsg("确认密码必须和新密码一致", confirmPwdInput, confirmPwdInput)
        return;
    } else {
        removeErrorMsg(confirmPwdInput);
    }

    showToast('修改成功', 1, 2, '../login.html')

}