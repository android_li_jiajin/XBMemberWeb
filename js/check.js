/*
 * @Description: 
 * @Author: lijiajin
 * @Date: 2020-07-06 09:04:53
 * @LastEditTime: 2020-07-08 10:24:47
 * @LastEditors: lijiajin
 */

// 只能输入数字及字母，且输入字符位数最大为20位
function checknl20(value) {
    var test = /^[0-9a-zA-Z]{0,20}$/;
    return test.test(value);
}

// 正整数
function checkPositiveInteger(value) {
    var test = /^[1-9]\d*$/;
    return test.test(value);
}

// 正整数 最大100
function checkPositiveInteger2(value) {
    var test = /^([1-9][0-9]{0,1}|100)$/;
    return test.test(value);
}

// 正整数 最大99
function checkPositiveInteger3(value) {
    var test = /^([1-9][0-9]{0,1}|99)$/;
    return test.test(value);
}

// 正整数或0 最大100
function checkPositiveInteger3(value) {
    var test = /^([0-9][0-9]{0,1}|100)$/;
    return test.test(value);
}

// 正整数 和 0
function checkPositiveIntegerAndZero(value) {
    var test = /^(0|\+?[1-9][0-9]*)$/;
    return test.test(value);
}

// 输入实时限制
function keyUp(obj) {
    // 0或正整数
    // onkeyup="value=(value.replace(/\D/g,'')==''?'':parseInt(value))"
}

// 正数 和 0
function checkPositiveNumberAndZero(value) {
    var test = /^[+]{0,1}(\d+)$|^[+]{0,1}(\d+\.\d+)$/;
    return test.test(value);
}

// 正数或0，小数点后最多两位
function xiaoshutwo(value) {
    var test = /^(([1-9]{1}\d*)|(0{1}))(\.\d{0,2})?$/;
    return test.test(value);
}

// 正数，小数点后最多两位
function xiaoshutwo2(value) {
    var test = /^(([1-9]{1}\d*)|(0{1}))(\.\d{0,2})?$/;
    if (value == "0") {
        return false;
    }
    return test.test(value);
}

// 请填写英文字母（A-Z，a-z)或数字（0-9）组合
function checknl(value) {
    var test = /[0-9a-zA-Z]+/;
    return test.test(value);
}

// 请填写正确的手机号
function checkPhone(value) {
    var test = /^1[3|4|5|6|7|8|9][0-9]{9}$/;
    return test.test(value);
}

// 请填写正确的邮箱
function checkEmail(value) {
    // var test = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
    if (value.indexOf("@") == -1) {
        okable = false;
    }
    return true;
}

// 请填写手机号  11位数字
function checkPhone11(value) {
    // var test = /^\d{11}|\d{13}$/;
    var test = /^\d{11}$/;

    return test.test(value);
}

// 请填写正确的邮箱们
function checkEmails(value) {
    if(value.indexOf("，") == -1){
        return false;
    }
    var arr = value.split(",");
    var okable = true;
    var index = 0;
    for (j = 0; j < arr.length; j++) {
        if(arr[j]){
            index = index + 1;
        }
        if (arr[j] && arr[j].indexOf("@") == -1) {
            okable = false;
        }
    }
    if(index > 6){
        return false;
    }
    return okable;
}

// 请填写正确的url
function checkURL(value) {
    var test = /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/;
    return test.test(value);
}

// 包含数字、字母、特殊字符若干种（不可有汉字）
function checknls(value) {
    var test1 = /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[~!@#$%^&*])[\da-zA-Z~!@#$%^&*]{1,}$/;
    var test2 = /^[A-Za-z0-9]+$/;
    var test3 = /^[0-9]*$/;
    var test4 = /^[a-zA-Z]*$/;
    var test5 = /^[\u4e00-\u9fa5]{1,}$/;
    // console.log("-----------")
    // console.log(test1.test(value))
    // console.log(test2.test(value))
    // console.log(test3.test(value))
    // console.log(test4.test(value))
    // console.log(test5.test(value))
    return !test5.test(value);
}

// 包含数字、特殊字符若干种
function checkns(value) {
    var test1 = /^(?=.*[0-9])((?=.*[a-z])[0-9a-z]+|(?=.*[!@#$%^&*])[0-9!@#$%^&*]+)$/;
    var test2 = /^[A-Za-z0-9]+$/;
    var test3 = /^[0-9]*$/;
    var test4 = /^[a-zA-Z]*$/;
    var test5 = /^[\u4e00-\u9fa5]{1,}$/;
    // console.log("-----------")
    // console.log(test1.test(value))
    // console.log(test2.test(value))
    // console.log(test3.test(value))
    // console.log(test4.test(value))
    // console.log(test5.test(value))
    return true;
}

// 请不要输入非英文字符
function checkIsEnglish(value) {
    var test = /^[\d\w\x21-\x2f\x3a-\x40\x5b-\x60\x7B-\x7F]+$/;
    return test.test(value);
}

// 请填写正确的邮政编码
function checkIsZipCode(value) {
    var test = /^[0-9]{6}$/;
    return test.test(value);
}

// 请填写正确的国际区号，如+86
function checkIsInteCode(value) {
    var test = /^[+][0-9]+$/;
    return test.test(value);
}

