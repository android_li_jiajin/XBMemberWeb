/*
 * @Description: 
 * @Author: lijiajin
 * @Date: 2020-07-06 09:04:53
 * @LastEditTime: 2020-07-08 10:24:47
 * @LastEditors: lijiajin
 */

var layer;
var page_loading_gif;
layui.use('layer', function () {
    layer = layui.layer;
    $("body").prepend('<div id="page_loading"></div>');
    showPageLoading();
});

$(window).load(function () {
    hidePageLoading();
});


// 开启ajax加载
function showLoading() {
    layer.close(page_loading_gif);
    page_loading_gif = layer.load(2, {skin: "layer-page-loading", anim: -1, shade: [0.5, '#fff']});
}


// 关闭ajax加载
function hideLoading() {
    if (layer && page_loading_gif) {
        layer.close(page_loading_gif);
    }
}

// 开启页面加载
function showPageLoading() {
    var opts = {

        lines: 9, // The number of lines to draw

        length: 0, // The length of each line

        width: 10, // The line thickness

        radius: 15, // The radius of the inner circle

        corners: 1, // Corner roundness (0..1)

        rotate: 0, // The rotation offset

        color: '#000', // #rgb or #rrggbb

        speed: 1, // Rounds per second

        trail: 60, // Afterglow percentage

        shadow: false, // Whether to render a shadow

        hwaccel: false, // Whether to use hardware acceleration

        className: 'spinner', // The CSS class to assign to the spinner

        zIndex: 2e9, // The z-index (defaults to 2000000000)

        top: 'auto', // Top position relative to parent in px

        left: 'auto' // Left position relative to parent in px

    };

    var target = document.getElementById('page_loading');

    var spinner = new Spinner(opts).spin(target);
}

// 翻译弹窗
function showPageLoading2() {
    var opts = {

        lines: 9, // The number of lines to draw

        length: 0, // The length of each line

        width: 12, // The line thickness

        radius: 25, // The radius of the inner circle

        corners: 1, // Corner roundness (0..1)

        rotate: 0, // The rotation offset

        color: '#000', // #rgb or #rrggbb

        speed: 1, // Rounds per second

        trail: 60, // Afterglow percentage

        shadow: false, // Whether to render a shadow

        hwaccel: false, // Whether to use hardware acceleration

        className: 'spinner', // The CSS class to assign to the spinner

        zIndex: 2e9, // The z-index (defaults to 2000000000)

        top: 'auto', // Top position relative to parent in px

        left: 'auto' // Left position relative to parent in px

    };

    var target = document.getElementById('page_loading');

    var spinner = new Spinner(opts).spin(target);
}

// 关闭页面加载
function hidePageLoading() {
    $("#page_loading").remove();
}

