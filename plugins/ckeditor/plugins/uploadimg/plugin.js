(function () {
    b = 'uploadimg';
    CKEDITOR.plugins.add(b, {
        init: function (a) {
            a.addCommand(b, CKEDITOR.plugins.autoformat.commands.autoformat);
            a.ui.addButton('uploadimg', {
                label: "选择图片",
                command: 'uploadimg',
                icon: this.path + "/images/image.png"
            });
        }
    });

    CKEDITOR.plugins.autoformat = {
        commands: {
            autoformat: {
                exec: function (editor) {
                    formatText(editor);
                }
            }
        }
    };

    //执行的方法
    function formatText(editor) {
        layer.open({
            type: 2,
            title: '图片上传',
            skin: 'layui-layer-picture-bank', //自定义样式
            area: ['80%', '80%'],
            content: '/index.php/admin/image_bank/popImage?pic_bank_limit_num=20',
            btn: ['确定', '取消'],
            yes: function (index, layero) {
                layer.close(index);
                var body = layer.getChildFrame('body', index);
                var selectedLi = body.find(".img-list").find("li.selected");
                selectedLi.each(function (index, item) {
                    var imageData = $(item).find("img").attr("src");
                    // 创建img标签
                    var image = editor.document.createElement('img');
                    // 给img标签设置class类
                    image.setAttribute('class', 'insert-image');
                    // 将图片数据赋予img标签
                    image.setAttribute('src', imageData);
                    // 利用ckeditor提供的接口将标签插入到富文本框中
                    editor.insertElement(image);
                })
            },
        });
    }
})();